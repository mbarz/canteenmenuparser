export class Restaurant {
    rid: number;
    sto: string;
}

export module Restaurant {
    export const BWG: Restaurant = {
        rid: 133021011,
        sto: 'bwg_a'
    }
    export const BLN_K: Restaurant = {
        rid: 131021011,
        sto: 'bln_k'
    }
    export const MCH_P: Restaurant = {
        rid: 122051011,
        sto: 'mch_p'
    }
}